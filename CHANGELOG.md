## 1.0.2

- 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）

## v1.0.1

- 修复解压压缩的问题
- api8升级到api9，并转换为stage模型

## v1.0.0


- epublib 读写电子书功能均已实现
