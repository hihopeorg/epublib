/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class OPFTags {
    public static metadata = "metadata";
    public static meta = "meta";
    public static manifest = "manifest";
    public static packageTag = "package";
    public static itemref = "itemref";
    public static spine = "spine";
    public static reference = "reference";
    public static guide = "guide";
    public static item = "item";
}

export default OPFTags