/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class PackageDocumentBase {
    public static BOOK_ID_ID: string = "BookId";
    public static NAMESPACE_OPF: string = "http://www.idpf.org/2007/opf";
    public static NAMESPACE_DUBLIN_CORE: string = "http://purl.org/dc/elements/1.1/";
    public static PREFIX_DUBLIN_CORE: string = "dc";
    public static PREFIX_OPF: string = "opf";
    public static dateFormat: string = "yyyy-MM-dd";
}

export default PackageDocumentBase