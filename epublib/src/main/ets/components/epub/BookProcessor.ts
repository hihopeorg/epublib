/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import Book from '../domain/Book';
/**
 * Post-processes a book.
 * 
 * Can be used to clean up a book after reading or before writing.
 * 
 * @author paul
 *
 */

class BookProcessor {

    //TODO
    //	/**
    //	 * A BookProcessor that returns the input book unchanged.
    //	 */
    //	public BookProcessor IDENTITY_BOOKPROCESSOR = new BookProcessor() {
    //
    //		@Override
    //		public Book processBook(Book book) {
    //			return book;
    //		}
    //	};
    //
    //	Book processBook(Book book);

    /**
      * A BookProcessor that returns the input book unchanged.
      */
    //  public IDENTITY_BOOKPROCESSOR: BookProcessor = new BookProcessor();

    public processBook(book: Book): Book {
        return book;
    }
}

export default BookProcessor