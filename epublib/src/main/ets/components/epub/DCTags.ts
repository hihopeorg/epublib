/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class DCTags {
    public static title = "title";
    public static creator = "creator";
    public static subject = "subject";
    public static description = "description";
    public static publisher = "publisher";
    public static contributor = "contributor";
    public static date = "date";
    public static type = "type";
    public static format = "format";
    public static identifier = "identifier";
    public static source = "source";
    public static language = "language";
    public static relation = "relation";
    public static coverage = "coverage";
    public static rights = "rights";
}

export default DCTags;