/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class OPFAttributes {
    public static uniqueIdentifier = "unique-identifier";
    public static idref = "idref";
    public static OPF_name = "name";
    public static content = "content";
    public static type = "type";
    public static href = "href";
    public static linear = "linear";
    public static event = "event";
    public static role = "role";
    public static file_as = "file-as";
    public static id = "id";
    public static media_type = "media-type";
    public static title = "title";
    public static toc = "toc";
    public static version = "version";
    public static scheme = "scheme";
    public static property = "property";
}

export default OPFAttributes